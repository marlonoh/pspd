/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.molaya.utils;

import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author molaya
 */
public class Utilities {

	private Random RANDOM = new SecureRandom();
	private int PASSWORD_LENGTH = 8;

	public void setGeneratorPasswordLength(int length) {
		PASSWORD_LENGTH = length;
	}

	public String generatePassword() {
		String uppercase = "BCDFGHJKLMNPQRSTVWXYZ";
		String numbers = "0123456789";
		String characters = "#!@_*.$&";
		String[] list = { "uli", "oga", "efa", "ara", "ege", "ime", "ola",
				"aro", "alo", "uli", "ero", "efe", "wer", "asc", "yui", "uyi",
				"omi", "emu", "alo", "ebo" };
		String pw = "";
		while (pw.length() < PASSWORD_LENGTH) {
			int ind = RANDOM.nextInt(20);
			int index = (int) (RANDOM.nextDouble() * uppercase.length());
			pw += uppercase.substring(index, index + 1);
			pw += list[ind];
			ind = RANDOM.nextInt(20);
			pw += list[ind];
			index = (int) (RANDOM.nextDouble() * numbers.length());
			pw += numbers.substring(index, index + 1);
			index = (int) (RANDOM.nextDouble() * characters.length());
			pw += characters.substring(index, index + 1);
		}
		return pw;
	}
}
