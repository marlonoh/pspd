package org.gbross.pspd.util;


public class StdOutHandle {
	private static String character = "*";
	private static String line = "* *********************************************************";

	public static void start(String log) {
		if (log == null)
			log = "";
		System.out.println(line);
		System.out.println(character + " STARTING APPLICATION " + log + "...");
		System.out.println(line);
	}

	public static void add(String log) {
		String charable = character + " ";
		System.out.println(charable + log);
	}

	public static void end(String log) {
		System.out.println(line);
		System.out.println(character + " APPLICATION " + log + " STARTED...");
		System.out.println(line);
	}

	public static void info(String log) {
		System.out.println(line);
		System.out.println(character + " INFO: " + log + "...");
		System.out.println(line);
	}

	public static void error(String log) {
		System.err.println(line);
		System.err.println(character + " ERROR: ¡" + log + "!");
		System.err.println(line);
	}
	public static void alert(String log) {
		System.err.println(line);
		System.err.println(character + " ALERT!: " + log + ".");
		System.err.println(line);
	}

}



