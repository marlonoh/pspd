package org.gbross.pspd.repository;

import org.gbross.pspd.model.UserApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier("jdbcPspd")
	protected JdbcTemplate pspd;
	
	@Autowired
	@Qualifier("jdbcCiaUno")
	protected JdbcTemplate ciaUno;

	/* DAO METHOD */
	public UserApp getItem(long id) {
		return pspd.queryForObject("LeeUsuarios ?", userAppMapper, id);
	}
	
	public List<UserApp> getUsers(){
		return pspd.query("LeeUsuarios ",userAppMapper);
	}
	
	public UserApp getTest(long id){
		return ciaUno.queryForObject("SELECT * FROM referencias WHERE referencia=? and bodega=?", testMapper,"22000456510321","01");
		
	}

	/* Mapper SQL to ITEM */
	private static final RowMapper<UserApp> userAppMapper = new RowMapper<UserApp>() {
		public UserApp mapRow(ResultSet rs, int rowNum) throws SQLException {
			System.out.println("*" + rs.getString("usuario"));
			UserApp item = new UserApp(rs.getString("usuario"),
					rs.getString("clave"), rs.getString("nombre"),
					rs.getString("role"), rs.getInt("idUsuario"));

			return item;
		}
	};
	
	/* Mapper SQL to TEST */
	private static final RowMapper<UserApp> testMapper = new RowMapper<UserApp>() {
		public UserApp mapRow(ResultSet rs, int rowNum) throws SQLException {
			System.out.println("* Leyendo db");
			System.out.println("*" + rs.getString("referencia") + " * descripcion: "+rs.getString("descripcion") +"* bodega" + rs.getString("bodega"));
			UserApp item = new UserApp(rs.getString("referencia"),
					"clave", rs.getString("descripcion"),
					"bodega", 1);
			return item;
		}
	};

	public UserApp getUserApp(String username) {
		/*Retorna UserApp Class*/
		return pspd.queryForObject("LeeUsuarios ?", userAppMapper, username);
	}


}