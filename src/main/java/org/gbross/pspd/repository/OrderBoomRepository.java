package org.gbross.pspd.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.gbross.pspd.model.OrderBoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class OrderBoomRepository {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier("jdbcPspd")
	protected JdbcTemplate pspd;
	
	@Autowired
	@Qualifier("jdbcCiaUno")
	protected JdbcTemplate ciaUno;

	@Autowired
	@Qualifier("jdbcCiaSigte")
	protected JdbcTemplate ciaSigte;
	
	private List<Map<String, Object>> rows = null;
	
	/* DAO METHOD */
	public List<OrderBoom> getItem(long id) {
		//rows = getInventoryOne().forEach(action);;
		return pspd.query("QUERY FOR BOOM table", new Object[]{}, orderBoomMapper);
	}
	
	private List<Map<String, Object>> getInventoryOne() {	
		return ciaUno.queryForList("QUERY FOR INVENTARIO", new Object[]{});
	}
	
	private List<Map<String, Object>> getInventoryTwo() {	
		return ciaSigte.queryForList("QUERY FOR INVENTARIO", new Object[]{});
	}
	
	/* Mapper SQL to ITEM */
	private static final RowMapper<OrderBoom> orderBoomMapper = new RowMapper<OrderBoom>() {
		public OrderBoom mapRow(ResultSet rs, int rowNum) throws SQLException {
			System.out.println("* Leyendo db");
			System.out.println("*" + rs.getString("usuario"));
			OrderBoom item = new OrderBoom();
			return item;
		}
	};

}
