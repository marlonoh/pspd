package org.gbross.pspd.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.gbross.pspd.model.Producto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SearchItemRepository {

	@Autowired
	@Qualifier("jdbcPspd")
	protected JdbcTemplate pspd;

	@Autowired
	@Qualifier("jdbcCiaUno")
	protected JdbcTemplate ciaUno;

	@Autowired
	@Qualifier("jdbcCiaSigte")
	protected JdbcTemplate ciaSigte;

	private List<Producto> rowsCiaSigte = null;
	private List<Producto> rowsCiaPspd = null;
	private List<Producto> rowsCiaUno = null;

	/* Retorna Producto */
	public List<Producto> getProducto() {
		// rowsCiaPspd = getProductoSigte(); // Consulta de Producto en ciaSigte
		rowsCiaPspd = getReservas(); // Consulta de Producto en PSPD
		rowsCiaUno = ciaUno
				.query("select referencia, descripcion,\n"
						+ "(sum(\"saldo inicial mes\")+sum(\"entradas mes\")-sum(\"salidas mes\")) as cantidad\n"
						+ "from referencias\n" + "where bodega='01' \n"
						+ "group by referencia,descripcion;", productMapper);
		// rowsCiaUno.forEach((v)->System.out.println("Item : " + k +
		// " Count : " + v));

		rowsCiaUno.forEach((row) -> {
			row.setDescripcion(row.getDescripcion().replace("�", "^2"));
//			System.out.println("ROW:" + row.getReferencia() + " #Count : "
//					+ row.getCant());
			rowsCiaPspd.forEach((ro) -> {
//				System.out.println("RO:" + ro.getReferencia() + " #RO Count : "
//						+ ro.getCant());
				if (row.getReferencia().contains(ro.getReferencia())) {
					row.setCant(row.getCant() - ro.getCant());
				}
			});
		});

		return rowsCiaUno;

	}
	
	public List<Producto> getInventorio() {
		return ciaUno
				.query("select referencia, descripcion,\n"
						+ "(sum(\"saldo inicial mes\")+sum(\"entradas mes\")-sum(\"salidas mes\")) as cantidad\n"
						+ "from referencias\n"
						+ "where (\"saldo inicial mes\"+\"entradas mes\"-\"salidas mes\")<>0\n"
						+ "group by referencia,descripcion;", productMapper);
	}

	// private List<Producto> getProductoCiaSigte(){
	// return ciaSigte.query("select referencia, descripcion,\n"
	// +
	// "(sum(\"saldo inicial mes\")+sum(\"entradas mes\")-sum(\"salidas mes\")) as cantidad\n"
	// + "from referencias\n" + "where bodega='01' \n"
	// + "group by referencia,descripcion;",
	// productMapper);
	// }

	public List<Producto> getReservas() {
		return pspd.query("LeeReservas", productMapper);
	}

	/* Mapper SQL to ITEM */
	private static final RowMapper<Producto> productMapper = new RowMapper<Producto>() {
		public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
			Producto item = new Producto(rs.getString("referencia"),
					rs.getString("descripcion"), rs.getDouble("cantidad"));

			return item;
		}
	};

}
