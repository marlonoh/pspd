package org.gbross.pspd.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.gbross.pspd.model.OrderBoom;
import org.gbross.pspd.model.ProductBoom;
import org.gbross.pspd.model.Producto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class BoomRepository {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private SearchItemRepository search;

	@Autowired
	@Qualifier("jdbcPspd")
	protected JdbcTemplate pspd;

	@Autowired
	@Qualifier("jdbcCiaUno")
	protected JdbcTemplate ciaUno;

//	@Autowired
//	@Qualifier("jdbcCiaSigte")
//	protected JdbcTemplate ciaSigte;

	private List<Producto> reservas = null;
	private List<Producto> inventarios = null;

	/* DAO METHOD */
	public List<ProductBoom> getProductBoom(String ref) {
		List<ProductBoom> pBoom = pspd.query("LeeBoomProducto ?",
				productBoomMapper, ref);
		reservas = search.getReservas();
		inventarios = search.getInventorio();
		pBoom.forEach((row) -> {
			System.out.println("ROW:" + row.getReferencia() + " #Count : "
					+ row.getStock());
			inventarios.forEach((in) -> {
				System.out.println("IN:" + in.getReferencia() + " #IN Count : "
						+ in.getCant());
				if (row.getReferencia().contains(in.getReferencia())) {
					row.setStock(row.getStock() + in.getCant());
				}
			});
			reservas.forEach((res) -> {
				System.out.println("RES:" + res.getReferencia()
						+ " #RES Count : " + res.getCant());
				if (row.getReferencia().contains(res.getReferencia())) {
					row.setStock(res.getCant() - row.getStock());
				}
			});
			row.setStock(row.getStock() / row.getReceta());

		});

		return pBoom;
	}

	/* Mapper SQL to ITEM */
	private static final RowMapper<ProductBoom> productBoomMapper = new RowMapper<ProductBoom>() {
		public ProductBoom mapRow(ResultSet rs, int rowNum) throws SQLException {
			System.out.println("* BOOM PRODUCTO");
			System.out.println("*" + rs.getString("referencia"));
			ProductBoom item = new ProductBoom(rs.getString("referencia"),
					rs.getDouble("receta"), rs.getDouble("stock"));
			return item;
		}
	};

}
