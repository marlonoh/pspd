package org.gbross.pspd.security;

import java.util.ArrayList;

import org.gbross.pspd.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;



@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	// @Autowired
	// AuthenticationProvider authenticationProvider;
	@Autowired
	UserDetailsServiceImpl serviceAuth;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		ArrayList<Role> roles = serviceAuth.getALLRoles();
		http.authorizeRequests()
				.antMatchers("/resources/**", "/images/**", "/css/**", "/assets/**", "/apijson/**",
						"/bootstrap/**", "/", "/home", "/test**", "/contact","/js/**")
				.permitAll();

		http.authorizeRequests().antMatchers("/users**", "/reports**", "/config")
				.hasAnyAuthority("ROOT").anyRequest().authenticated().and()
				.formLogin().loginPage("/login").failureUrl("/?error")
				.permitAll().and().logout().permitAll()
				.logoutSuccessUrl("/?logout").and().exceptionHandling()
				.accessDeniedPage("/403");

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		//org.gbross.pspd.util.StdOutHandle.add("Configuring authentication roles");
		//org.gbross.pspd.util.StdOutHandle.end("ADTOOLS");
		auth.userDetailsService(serviceAuth);
//		auth.inMemoryAuthentication()
//		.withUser("planeacion").password("planeacion").roles("TECH").and().withUser("administrator").password("sincelej0").roles("ROOT");
//		 .userDetailsService(new UserDetailsServiceImpl());
		// .authenticationProvider(authenticationProvider);
	}
	
	// @Override
	// protected void configure(AuthenticationManagerBuilder auth) throws
	// Exception {
	// auth.userDetailsService(serviceAuth);
	// }
	//
	// @Override
	// public UserDetailsService userDetailsServiceBean() {
	// return serviceAuth;
	// }
}
