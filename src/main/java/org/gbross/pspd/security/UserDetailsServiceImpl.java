package org.gbross.pspd.security;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gbross.pspd.dao.UserDAO;
import org.gbross.pspd.model.DataBase;
import org.gbross.pspd.model.Role;
import org.gbross.pspd.model.UserApp;
import org.gbross.pspd.repository.UserRepository;
import org.gbross.pspd.util.StdOutHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private UserRepository users;


	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		List<GrantedAuthority> authList = null;
		GrantedAuthority authority = null;
		UserDetails userDetails = null;
		UserApp userInfo = null;
		String role = "TECH";
		if (users != null) {
			userInfo = users.getUserApp(username);
			StdOutHandle.info("UserDetailsService : " + userInfo.getName() + " as "
					+ userInfo.getRole() + " Password:" + userInfo.getPassword());

			authority = new SimpleGrantedAuthority(userInfo.getRole());
		} else {
			StdOutHandle.error("ERROR DAO");
			throw new UsernameNotFoundException("Error null username");
		}
		
		userDetails = (UserDetails) new User(userInfo.getUsername(),
				userInfo.getPassword(), true, true, true, true,
				AuthorityUtils.createAuthorityList(role));
			return userDetails;
	}

//	public ArrayList<Role> getALLRoles() {
//		StdOutHandle.info("Getting all roles UDS");
//		return dao.listRoles();
//	}

}