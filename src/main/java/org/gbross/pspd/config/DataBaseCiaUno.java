package org.gbross.pspd.config;

import org.springframework.boot.autoconfigure.jdbc.TomcatDataSourceConfiguration; 
import org.springframework.boot.context.properties.ConfigurationProperties; 
import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.jdbc.core.JdbcTemplate; 
import pervasive.*;
 
import javax.sql.DataSource; 
 
@Configuration 
@ConfigurationProperties("spring.ds_ciauno") 
//@ConfigurationProperties(locations = "classpath:mail.properties", ignoreUnknownFields = false, prefix = "mail")
public class DataBaseCiaUno extends TomcatDataSourceConfiguration { 
 
    @Bean(name = "dsCiaUno") 
    public DataSource dataSource() { 
    	System.out.println("## DRIVER: "+super.getDriverClassName()+"\n## URL: "+super.getUrl());
        return super.dataSource(); 
    } 
 
    @Bean(name = "jdbcCiaUno") 
    public JdbcTemplate jdbcTemplate(DataSource dsCiaUno) { 
        return new JdbcTemplate(dsCiaUno); 
    } 
} 

