package org.gbross.pspd.config;
import org.springframework.boot.autoconfigure.jdbc.TomcatDataSourceConfiguration; 
import org.springframework.boot.context.properties.ConfigurationProperties; 
import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.jdbc.core.JdbcTemplate; 
 
import javax.sql.DataSource; 
 
@Configuration 
//@ConfigurationProperties(locations = "classpath:databas_pspd.properties") 
@ConfigurationProperties("spring.ds_pspd") 
//@ConfigurationProperties(locations = "classpath:mail.properties", ignoreUnknownFields = false, prefix = "mail")
public class DataBasePspd extends TomcatDataSourceConfiguration { 
 
    @Bean(name = "dsPspd")
    public DataSource dataSource() { 
        return super.dataSource(); 
    } 
 
    @Bean(name = "jdbcPspd") 
    public JdbcTemplate jdbcTemplate(DataSource dsPspd) { 
        return new JdbcTemplate(dsPspd); 
    } 
} 
