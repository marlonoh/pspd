package org.gbross.pspd.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.pspd.repository.BoomRepository;
import org.gbross.pspd.repository.UserRepository;
import org.gbross.pspd.database.PervasiveConnection;
import org.gbross.pspd.database.SqlServerConnection;
import org.gbross.pspd.model.SideBar;
import org.gbross.pspd.model.TableFormat;
import org.gbross.pspd.model.Thead;
import org.gbross.pspd.model.UserApp;
import org.gbross.pspd.util.StdOutHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("test")
public class TestController {
	@Autowired
	private UserRepository users;

	@Autowired
	private BoomRepository boom;

	List<SideBar> sidebar;

	private SqlServerConnection sql = new SqlServerConnection();
	private PervasiveConnection psql = new PervasiveConnection();
	
	@PostConstruct
	public void init() {
		StdOutHandle.add("* Initializing HomeController");
		sidebar = new ArrayList<SideBar>();
		sidebar.add(new SideBar("DASHBOARD", "/dashboard", 0));
		sidebar.add(new SideBar("SEARCHITEM", "/searchItem", 1));
//		sidebar.add(new SideBar("ORDERS", "/orders", 2));
//		sidebar.add(new SideBar("PROFILE", "/profile", 3));
//		sidebar.add(new SideBar("USERS", "/users", 4));
//		sidebar.add(new SideBar("ROLES", "/roles", 5));
//		sidebar.add(new SideBar("BOOMPRODS", "/boomprods", 6));
//		sidebar.add(new SideBar("BOOMORDER", "/boomorder", 7));

	}

	private void asignActiveUrl(long id) {
		sidebar.forEach(p -> {
			if (p.getId() == id)
				p.setActive("active");
			else
				p.setActive("");
		});
	}

	@RequestMapping("/dashboard")
	public String dash(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/dashboard";
	}
	
	@RequestMapping("/profile")
	public String users(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/user";
	}

	@RequestMapping("/maps")
	public String maps(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/maps";
	}
	
	@RequestMapping("/icons")
	public String icons(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/icons";
	}
	
	@RequestMapping("/notifications")
	public String notifications(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/notifications";
	}
	
	@RequestMapping("/typography")
	public String typography(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/typography";
	}
	
	@RequestMapping("/table")
	public String table(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "red") String style,
			Model model) {

		model.addAttribute("style", style);
		return "x-light/table";
	}
	
	@RequestMapping("/testdash")
	public String slash(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "black") String style,
			Model model) {
		// model.addAttribute("userapp",
		// users.getUserApp(request.getRemoteUser()));
		model.addAttribute("style", style);
		return "x-light/dashboard";
	}

	@RequestMapping("/searchItem")
	public String consultaProducto(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "black") String style,
			Model model) {
		
		int pageId = 1;
		asignActiveUrl(pageId);
		
		/* Table Configurations */

		List<Thead> theads = new ArrayList<Thead>();
		theads.add(new Thead("ID", "id"));
		theads.add(new Thead("Referencia", "referencia"));
		theads.add(new Thead("Descripción", "descripcion"));
		theads.add(new Thead("Cantidad", "cant"));

		TableFormat table = new TableFormat(true, true, true, true, true, true,
				true, "referencia", false, "client",
				"apijson/tblConsultaProducto", true);

		model.addAttribute("table", table);
		model.addAttribute("theads", theads);
		
		/* END Table*/
		
		/* Page Attributes */
		model.addAttribute("userapp", users.getUserApp(request.getRemoteUser()));
		// new UserApp("molaya","xxxxxxxx","Marlon Olaya","ROOT", 0));
		
		model.addAttribute("sidebar", sidebar);
		model.addAttribute("page", sidebar.get(pageId));
		model.addAttribute("style", style);
		/* END Page */

		return "x-light/pedidos";
	}

	
	@RequestMapping("/boomProducto")
	public String getBoomProducto(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			@RequestParam(value = "referencia", required = false, defaultValue = " ") String referencia,
			Model model) {
		int pageId = 1;
		asignActiveUrl(pageId);
		
		/* Table Configurations */

		List<Thead> theads = new ArrayList<Thead>();
		theads.add(new Thead("ID", "id"));
		theads.add(new Thead("Referencia", "referencia"));
		theads.add(new Thead("Descripción", "descripcion"));
		theads.add(new Thead("Cantidad", "cant"));

		TableFormat table = new TableFormat(true, true, true, true, true, true,
				true, "referencia", false, "client",
				"apijson/tblBoomProducto?referencia="+referencia, true);

		model.addAttribute("table", table);
		model.addAttribute("theads", theads);
		
		/* END Table*/
		
		/* Page Attributes */
		model.addAttribute("userapp", users.getUserApp(request.getRemoteUser()));
		// new UserApp("molaya","xxxxxxxx","Marlon Olaya","ROOT", 0));
		
		model.addAttribute("sidebar", sidebar);
		model.addAttribute("page", sidebar.get(pageId));
		model.addAttribute("style", style);
		/* END Page */

		model.addAttribute("prboom", boom.getProductBoom(referencia));
		return "base2";
	}

	@RequestMapping("/testdb")
	@ResponseBody
	public UserApp testdb(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		return users.getItem(0);
	}

	@RequestMapping("/testdb1")
	@ResponseBody
	public UserApp testdb1(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		return users.getTest(0);
	}

	@RequestMapping("/base")
	public String base(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp", users.getUserApp(request.getRemoteUser()));
		model.addAttribute("style", style);
		// TODO Add modelAtributte messages, and alerts
		return "base";
	}

	@RequestMapping("/testdashboard")
	public String testdash(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp", users.getUserApp(request.getRemoteUser()));
		model.addAttribute("style", style);
		// TODO Add modelAtributte messages, and alerts
		return "dashboard";
	}

	@RequestMapping("/basetest")
	public String basetest(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp", users.getUserApp(request.getRemoteUser()));
		model.addAttribute("style", style);
		// TODO Add modelAtributte messages, and alerts
		return "base2";
	}

	@RequestMapping("/contactus")
	public String contactus(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		model.addAttribute("style", style);
		return "frmIndex";
	}

	@RequestMapping("/aboutus")
	public String aboutus(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		model.addAttribute("userapp", request.getRemoteUser());
		return "dashboard";
	}

}
