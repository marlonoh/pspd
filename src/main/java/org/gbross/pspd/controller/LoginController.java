package org.gbross.pspd.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.pspd.dao.UserDAO;
import org.gbross.pspd.model.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String getLogout(Principal user, Model model) {
		model.addAttribute("msg", "Hi "+user.getName()+" No tiene permiso para acceder");
		return "My403";
	}
	
	@RequestMapping("/test")
	@Secured("TECH")
	@ResponseBody
	public String getTest(HttpSession session){
		return "My sessionId = " + session.getId();
	}
	
	@RequestMapping("/testing2")
	public String gettest2(HttpSession session, HttpServletRequest request) throws Exception{
		String username = request.getRemoteUser();
		return "hello";
	}
	

}
