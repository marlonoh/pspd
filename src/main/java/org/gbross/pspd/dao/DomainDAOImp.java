//package org.gbross.pspd.dao;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.gbross.pspd.database.SqliteConnection;
//import org.gbross.pspd.model.Domain;
//import org.gbross.pspd.model.UserApp;
//import org.gbross.pspd.util.StdOutHandle;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class DomainDAOImp implements DomainDAO {
//
//	private SqliteConnection db;
//	private Domain domain;
//
//	@Autowired
//	public DomainDAOImp(SqliteConnection db) {
//		this.db = db;
//		if (!this.db.isConnected()) {
//			this.db.connect();
//			StdOutHandle.add("Configuring domains on database");
//		}
//		this.domain = new Domain();
//	}
//
//	@Override
//	public void resetCredentials(Domain domain) {
//		String sql = "UPDATE TBLUSERS SET password=\"" + domain.getPassAd()
//				+ "\", username=\"" + domain.getUserAd() + "\", WHERE domain="
//				+ domain.getDomain() + "";
//		db.write(sql);
//	}
//
//	@Override
//	public void saveOrUpdate(Domain dom) {
//		Domain aux = null;
//		aux = getDomain(dom.getDomain());
//		String sql = null;
//		if (aux != null) {
//			// update
//			sql = "UPDATE TBLDOMAINS SET username=\"" + dom.getUserAd()
//					+ "\", password=\"" + dom.getPassAd() + "\", ipaddress=\""
//					+ dom.getIpAd() + "\", upn=\"" + dom.getUpnAd()
//					+ "\", ougroup='" + dom.getOuGroups() + "', logo='"
//					+ dom.getLogo() + "' " + "WHERE domain='" + aux.getDomain()
//					+ "'";
//		} else {
//			// insert
//			sql = "INSERT INTO TBLDOMAINS (username, password, ipaddress, upn, ougroup, logo, domain)"
//					+ " VALUES ('"
//					+ dom.getUserAd()
//					+ "', '"
//					+ dom.getPassAd()
//					+ "', '"
//					+ dom.getIpAd()
//					+ "', '"
//					+ dom.getUpnAd()
//					+ "', '"
//					+ dom.getOuGroups()
//					+ "', '"
//					+ dom.getLogo() + "','" + dom.getDomain() + "' )";
//		}
//		StdOutHandle.info("Adding DOMAIN: "+ dom.getDomain());
//		db.write(sql);
//	}
//
//	@Override
//	public Domain getDomain(String name) {
//		String sql = "SELECT * FROM TBLDOMAINS WHERE domain = '" + name + "'";
//		ResultSet rs = db.read(sql);
//		try {
//			if (rs.next())
//				return new Domain(rs.getString("domain"),
//						rs.getString("username"), rs.getString("password"),
//						rs.getString("ipaddress"), rs.getString("upn"),
//						rs.getString("ougroup"), rs.getString("logo"));
//		} catch (SQLException ex) {
//			StdOutHandle.error("Error Getting Domain: \n" + ex);
//		} catch (RuntimeException ex) {
//			return null;
//		}
//		return null;
//	}
//
//	@Override
//	public List<Domain> getAllDomains() {
//		String sql = "SELECT * FROM TBLDOMAINS";
//		ResultSet rs = db.read(sql);
//		List<Domain> doms = new ArrayList<Domain>();
//		try {
//			while (rs.next()) {
//				doms.add(new Domain(rs.getString("domain"), rs
//						.getString("username"), rs.getString("password"), rs
//						.getString("ipaddress"), rs.getString("upn"), rs
//						.getString("ougroup"), rs.getString("logo"), rs
//						.getInt("domainid")));
//
//			}
//			return doms;
//		} catch (SQLException ex) {
//			StdOutHandle.error("Error Getting List Of Domains: \n" + ex);
//		} catch (RuntimeException ex) {
//			return null;
//		}
//		return null;
//	}
//
//	@Override
//	public List<String> getAllDomainNames() {
//		String sql = "SELECT * FROM TBLDOMAINS";
//		ResultSet rs = db.read(sql);
//		List<String> doms = new ArrayList<String>();
//		try {
//			while (rs.next()) {
//				doms.add(rs.getString("domain"));
//			}
//			return doms;
//		} catch (SQLException ex) {
//			StdOutHandle.error("Error Getting List Domain Names: \n" + ex);
//		} catch (RuntimeException ex) {
//			return null;
//		}
//		return null;
//
//	}
//
//	@Override
//	public boolean isConnected() {
//		return db.isConnected();
//	}
//
//	@Override
//	public void delete(String name) {
//		String sql = "DELETE FROM TBLUSERS WHERE domain='" + name + "'";
//		db.write(sql);
//	}
//
//	@Override
//	public List<String> getUsernameByDomain(String domain) {
//		ResultSet rs = null;
//		String sql = "SELECT tu.useranme FROM TBLUSERS tu INNER JOIN TBLUSERS_DOM tud "
//				+ "on tud.userid=tu.userid INNER JOIN TBLDOMAINS td "
//				+ "on tud.domainid=td.domainid WHERE td.domain='"
//				+ domain
//				+ "'";
//
//		rs = db.read(sql);
//		List<String> users = new ArrayList<String>();
//		try {
//			while (rs.next()) {
//				StdOutHandle.info("Correct read role: "
//						+ rs.getString("username"));
//				users.add(rs.getString("username"));
//			}
//			return users;
//		} catch (SQLException ex) {
//			StdOutHandle.error("Error Getting Users by domain: \n" + ex);
//		} catch (RuntimeException ex) {
//			return null;
//		}
//		return null;
//	}
//
//	@Override
//	public boolean isUsernameOnDomain(String domain, String username) {
//		ResultSet rs = null;
//		String sql = "SELECT tu.username FROM TBLUSERS tu INNER JOIN TBLUSERS_DOM tud "
//				+ "on tu.userid=tud.userid INNER JOIN TBLDOMAINS td "
//				+ "on td.domainid=tud.domainid WHERE td.domain='"
//				+ domain
//				+ "' and tu.username='" + username + "'";
//		rs = db.read(sql);
//		try {
//			if (rs.next()
//					&& rs.getString("username").equalsIgnoreCase(username)) {
//				StdOutHandle.info("Correct Domain: " + domain + " for User: "
//						+ rs.getString("username"));
//				return true;
//			}
//		} catch (SQLException ex) {
//			StdOutHandle.error("Error Validating Domain by User: \n" + ex);
//		} catch (RuntimeException ex) {
//			return false;
//		}
//		return false;
//	}
//}
