package org.gbross.pspd.dao;

import java.util.List;

import org.gbross.pspd.model.Invoice;
import org.gbross.pspd.model.InvoiceItem;
import org.gbross.pspd.model.UserApp;
import org.springframework.stereotype.Component;

@Component
public interface InvoiceDAO {
		public void create(Invoice invoice);
		public Invoice read(int reference);
		public List<Invoice> readAll();
		public List<InvoiceItem> readAllItems(int reference);
		public boolean isConnected();
		public void delete(int reference);
		public void update(Invoice invoice);
		
}

