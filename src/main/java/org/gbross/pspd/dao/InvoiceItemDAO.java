package org.gbross.pspd.dao;

import java.util.List;

import org.gbross.pspd.model.Invoice;
import org.gbross.pspd.model.InvoiceItem;
import org.gbross.pspd.model.UserApp;
import org.springframework.stereotype.Component;

@Component
public interface InvoiceItemDAO {
		public void create(InvoiceItem items);
		public InvoiceItem read(int id);
		public List<InvoiceItem> readAll();
		public boolean isConnected();
		public void delete(int id);
		public void update(int id);
		
}

