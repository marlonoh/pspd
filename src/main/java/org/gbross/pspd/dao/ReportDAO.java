package org.gbross.pspd.dao;

import java.util.Date;
import java.util.List;

import org.gbross.pspd.model.ReportLogs;
import org.springframework.stereotype.Component;

@Component
public interface ReportDAO {
	public List<ReportLogs> getReports();
	public List<ReportLogs> getReportsBySamidAD(String samid);
	public List<ReportLogs> getReportsByUsername(String username);
	public List<ReportLogs> getReportsByDate(Date start, Date end);
}

