package org.gbross.pspd.model;

public class TableFormat {
	private boolean enablesearch;
	private boolean showrefresh;
	private boolean showtoggle;
	private boolean showcolumns;
	private boolean showexport;
	private boolean detailview;
	private boolean enablepag;
	private String idfield;
	private boolean showfooter;
	private String sidepag;
	private String srcurl;
	private boolean singleselect;
	
	public TableFormat(){}
	/**
	 * Default Values
	 */
	public TableFormat(boolean defaultValues){
		this.enablesearch = true;
		this.showrefresh = true;
		this.showtoggle = true;
		this.showcolumns = true;
		this.showexport = true;
		this.detailview = true;
		this.enablepag = true;
		this.idfield = "id";
		this.showfooter = true;
		this.sidepag = "server";
		this.srcurl = "apijson/tablePag";
		this.singleselect = true;
	}
	/**
	 * All Parameters Constructor
	 * @param enablesearch
	 * @param showrefresh
	 * @param showtoggle
	 * @param showcolumns
	 * @param showexport
	 * @param detailview
	 * @param enablepag
	 * @param idfield
	 * @param showfooter
	 * @param sidepag
	 * @param srcurl
	 * @param singleselect
	 */
	public TableFormat(boolean enablesearch, boolean showrefresh,
			boolean showtoggle, boolean showcolumns, boolean showexport,
			boolean detailview, boolean enablepag, String idfield,
			boolean showfooter, String sidepag, String srcurl,
			boolean singleselect) {
		super();
		this.enablesearch = enablesearch;
		this.showrefresh = showrefresh;
		this.showtoggle = showtoggle;
		this.showcolumns = showcolumns;
		this.showexport = showexport;
		this.detailview = detailview;
		this.enablepag = enablepag;
		this.idfield = idfield;
		this.showfooter = showfooter;
		this.sidepag = sidepag;
		this.srcurl = srcurl;
		this.singleselect = singleselect;
	}
	public boolean isEnablesearch() {
		return enablesearch;
	}
	public void setEnablesearch(boolean enablesearch) {
		this.enablesearch = enablesearch;
	}
	public boolean isShowrefresh() {
		return showrefresh;
	}
	public void setShowrefresh(boolean showrefresh) {
		this.showrefresh = showrefresh;
	}
	public boolean isShowtoggle() {
		return showtoggle;
	}
	public void setShowtoggle(boolean showtoggle) {
		this.showtoggle = showtoggle;
	}
	public boolean isShowcolumns() {
		return showcolumns;
	}
	public void setShowcolumns(boolean showcolumns) {
		this.showcolumns = showcolumns;
	}
	public boolean isShowexport() {
		return showexport;
	}
	public void setShowexport(boolean showexport) {
		this.showexport = showexport;
	}
	public boolean isDetailview() {
		return detailview;
	}
	public void setDetailview(boolean detailview) {
		this.detailview = detailview;
	}
	public boolean isEnablepag() {
		return enablepag;
	}
	public void setEnablepag(boolean enablepag) {
		this.enablepag = enablepag;
	}
	public String getIdfield() {
		return idfield;
	}
	public void setIdfield(String idfield) {
		this.idfield = idfield;
	}
	public boolean isShowfooter() {
		return showfooter;
	}
	public void setShowfooter(boolean showfooter) {
		this.showfooter = showfooter;
	}
	public String getSidepag() {
		return sidepag;
	}
	public void setSidepag(String sidepag) {
		this.sidepag = sidepag;
	}
	public String getSrcurl() {
		return srcurl;
	}
	public void setSrcurl(String srcurl) {
		this.srcurl = srcurl;
	}
	public boolean isSingleselect() {
		return singleselect;
	}
	public void setSingleselect(boolean singleselect) {
		this.singleselect = singleselect;
	}	
	
}
