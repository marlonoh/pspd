package org.gbross.pspd.model;

import java.util.List;

import org.gbross.pspd.util.StdOutHandle;

/**
 * @author molaya
 *
 */
public class UserApp {
	private String username;
	private String password;
	private String name;
	private String email;
	private int status;
	private int roleid;
	private int id;
	private String role;

	public UserApp() {

	}

	/* UserApp ALL VALUES */
	public UserApp(String username, String password, String name, String email,
			int status, int roleid, int id, String role) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.status = status;
		this.roleid = roleid;
		this.id = id;
		this.role = role;
	}

	public UserApp(String username, String password, String name, String role,
			int id) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.id = id;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;

	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserApp [username=" + username + ", password=" + password
				+ ", name=" + name + ", email=" + email + ", status=" + status
				+ ", roleid=" + roleid + ", id=" + id + ", role=" + role + "]";
	}

}
