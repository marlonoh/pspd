package org.gbross.pspd.model;

import org.springframework.stereotype.Component;

@Component
public class DataBase {

	private String driver;
	private String url;
	private String user;
	private String password;
	private String server;
	private String port;
	private String dataBase;

	public DataBase() {
		this.dataBase = "InvoiceDB.db";
		this.driver = "org.sqlite.JDBC";
		this.url = "jdbc:sqlite:db/" + this.dataBase;
		this.port = "8090";
		this.user = "";
		this.password = "";
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String d) {
		this.driver = driver;
	}

	public String getDataBase() {
		return dataBase;
	}

	public void setDataBase(String database) {
		this.dataBase = database;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPort() {
		return port;
	}

}
