package org.gbross.pspd.model;

import java.util.ArrayList;
import java.util.List;

import org.molaya.utils.RuntimeExec;

public class DsnObjectAd implements Comparable<DsnObjectAd>{
	
	private String name;
	private String office;
	private String company;
	private String dsn;
	private String samid;
	private String email;
	private String cc;
	private String type;
	private int id;
	
	public DsnObjectAd(String name, String office, String company,
			String dsn, String samid, String email, String cc, String type, int id) {
		super();
		this.id = id;
		this.name = name;
		this.office = office;
		this.company = company;
		this.dsn = dsn;
		this.samid = samid;
		this.email = email;
		this.cc = cc;
		this.type = type;
	}

	public DsnObjectAd(String name, String office, String company, String dsn,
			String samid, String email, String cc, String type) {
		super();
		this.name = name;
		this.office = office;
		this.company = company;
		this.dsn = dsn;
		this.samid = samid;
		this.email = email;
		this.cc = cc;
		this.type = type;
	}
	
	public DsnObjectAd(String name, String office, String company, String dsn,
			String samid, String email, String cc) {
		this.name = name;
		this.office = office;
		this.company = company;
		this.dsn = dsn;
		this.samid = samid;
		this.email = email;
		this.cc = cc;
	}
	public DsnObjectAd(String name, String office, String company, String dsn) {
		this.name = name;
		this.office = office;
		this.company = company;
		this.dsn = dsn;
	}
	public DsnObjectAd(String dsn){
		this.dsn = dsn;
		getValues(dsn);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDsn() {
		return dsn;
	}
	public void setDsn(String dsn) {
		this.dsn = dsn;
	}
	public String getSamid() {
		return samid;
	}
	public void setSamid(String samid) {
		this.samid = samid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private void getValues(String result){
		String cn = null, ou = null, dc = null;
		String[] rest = result.split("\n");
		String[] aux = null; 
		for (String test : rest) {
			aux = test.split("\"CN=")[1].split(",OU=");
			name = test.split("\"CN=")[1].split(",")[0];
			office = test.split("\"CN=")[1].split(",OU=")[aux.length-1].split(",")[0];
			company = test.split("\"CN=")[1].split(",OU=")[aux.length-1].split(",DC=")[1];
			System.out.println("*** "+ cn + " - "+ ou + " - " + dc);
		}
	} 
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DsnObjectAd))
			return false;

		DsnObjectAd that = (DsnObjectAd) obj;
		return this.samid.equals(that.samid);
	}

	@Override
	public int hashCode() {
		return this.samid.hashCode();
	}
	
	@Override
	public int compareTo(DsnObjectAd anotherDsnObjectAd) {
		 //returns -1 if "this" object is less than "that" object
	    //returns 0 if they are equal
	    //returns 1 if "this" object is greater than "that" object
		if (!(anotherDsnObjectAd instanceof DsnObjectAd))
		      throw new ClassCastException("A DsnAd object expected.");
		String samid = ((DsnObjectAd) anotherDsnObjectAd).getSamid();
		return this.samid.compareToIgnoreCase(samid);    
	}
}
