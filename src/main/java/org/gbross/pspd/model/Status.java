package org.gbross.pspd.model;

public class Status {
	private String reason;
	private String info;
	private String isOk;

	public String isOk() {
		return isOk;
	}
	public void setOk(String isOk) {
		this.isOk = isOk;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
}
