package org.gbross.pspd.model;

public class Producto {
	private String referencia;
	private String descripcion;
	private Double cant;
	
	public Producto(){}
	
	public Producto(String referencia, String descripcion, Double cant) {
		super();
		this.referencia = referencia;
		this.descripcion = descripcion;
		this.cant = cant;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Double getCant() {
		return cant;
	}
	public void setCant(Double cant) {
		this.cant = cant;
	}

	@Override
	public String toString() {
		return "Producto [referencia=" + referencia + ", descripcion="
				+ descripcion + ", cant=" + cant + "]";
	}
	
}
