package org.gbross.pspd.model;

/**
 * Model Attribute to pass for pages with Bootstrap Table.
 * @author molaya
 *
 */
public class Thead {
	private String tittle;
	private String field;
	
	public Thead(){}
	public Thead(String tittle, String field) {
		super();
		this.tittle = tittle;
		this.field = field;
	}
	
	public String getTittle() {
		return tittle;
	}
	public void setTittle(String tittle) {
		this.tittle = tittle;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
}
