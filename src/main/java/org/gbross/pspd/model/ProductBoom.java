package org.gbross.pspd.model;

public class ProductBoom {
	private String referencia;
	private Double receta;
	private Double stock;
	
	public ProductBoom() {
		super();
	}
	public ProductBoom(String referencia, Double receta, Double stock) {
		super();
		this.referencia = referencia;
		this.receta = receta;
		this.stock = stock;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Double getReceta() {
		return receta;
	}
	public void setReceta(Double receta) {
		this.receta = receta;
	}
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}
	@Override
	public String toString() {
		return "ProductBoom [referencia=" + referencia + ", receta=" + receta
				+ ", stock=" + stock + "]";
	}
}
