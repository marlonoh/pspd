package org.gbross.pspd.model;

/**
 * @author molaya
 *
 */
public class ObjectAD {
	private String samid;
	private String password;
	private String name;
	private String lastname;
	private String profile;
	private String office;
	private String type;
	private String cc;
	private String email;
	
	public ObjectAD(){
		
	}

	public ObjectAD(String samid, String password, String name, String office, String profile){
		this.samid = samid;
		this.password = password;
		this.name = name;
		this.profile = profile;
		this.office = office;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSamid() {
		return samid;
	}
	public void setSamid(String samid) {
		this.samid = samid;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
