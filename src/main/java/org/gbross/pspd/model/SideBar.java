package org.gbross.pspd.model;

public class SideBar {
	private String tittle;
	private String url;
	private String active;
	private long id;

	public SideBar(String tittle, String url, String active, long id) {
		super();
		this.tittle = tittle;
		this.url = url;
		this.active = active;
		this.id = id;
	}

	public SideBar(String tittle, String url, long id) {
		super();
		this.tittle = tittle;
		this.url = url;
		this.active = "";
	}

	public void setActiveUrl() {
		this.active = "active";
	}

	public void replace(SideBar newbar) {
		this.tittle = newbar.getTittle();
		this.url = newbar.getUrl();
		this.active = newbar.getActive();
		this.id = newbar.getId();
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
