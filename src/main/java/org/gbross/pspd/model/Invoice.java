package org.gbross.pspd.model;

import java.util.Date;
import java.util.List;

public class Invoice {
	private Date date;
	private String customer;
	private String reference; // the same id in the table
	private String status;
	private List<InvoiceItem> items;
	private String total;
	private String offer;
		
	public Invoice() {
		super();
	}
	public Invoice(String reference, Date date, String customer, String status,
			List<InvoiceItem> items) {
		super();
		this.date = date;
		this.customer = customer;
		this.reference = reference;
		this.status = status;
		this.items = items;
	}
	public Invoice(String reference, Date date, String customer, String status,
			List<InvoiceItem> items, String total, String offer) {
		super();
		this.date = date;
		this.customer = customer;
		this.reference = reference;
		this.status = status;
		this.items = items;
		this.total = total;
		this.offer = offer;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<InvoiceItem> getItems() {
		return items;
	}
	public void setItems(List<InvoiceItem> items) {
		this.items = items;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}
	
}
