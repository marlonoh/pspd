package org.gbross.pspd.model;

/**
 * Items in an specific Invoice
 * @author molaya
 *
 */
public class InvoiceItem {
	private int id;
	private int amount;
	private String name;
	private String detail;
	private String unitPrice;
	private int reference; 
	
	public InvoiceItem() {
		super();
	}
	public InvoiceItem(int id, int reference, int amount, String name, String detail, String unitPrice) {
		super();
		this.id = id;
		this.reference = reference;
		this.amount = amount;
		this.name = name;
		this.detail = detail;
		this.unitPrice = unitPrice;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getReference() {
		return reference;
	}
	public void setReference(int reference) {
		this.reference = reference;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	
}
