package org.gbross.pspd.model;

public class Role {
	private String rolename;
	private int roleid;
	
	public Role(){
		
	}
	
	public Role(int roleid, String rolename){
		this.rolename = rolename;
		this.roleid = roleid;
	}
	
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public int getRoleid() {
		return roleid;
	}
	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}
	
	@Override
	public String toString() {
		return "Role [rolename=" + rolename + ", roleid=" + roleid + "]";
	}

}
