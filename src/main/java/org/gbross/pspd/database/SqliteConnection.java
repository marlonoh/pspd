//package org.gbross.pspd.database;
//
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.sql.Connection;
//import java.sql.DatabaseMetaData;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//import org.gbross.pspd.model.DataBase;
//import org.gbross.pspd.util.StdOutHandle;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * This program demonstrates making JDBC connection to a SQLite database.
// * 
// * @author www.codejava.net
// *
// */
//@Component
//public class SqliteConnection {
//	private String url;
//	private String driver;
//	private String dbName;
//	private Statement stmt;
//	private Connection conn;
//
//	@Autowired
//	public SqliteConnection(DataBase db) {
//		url = db.getUrl();
//		driver = db.getDriver();
//		dbName = db.getDataBase();
//		this.conn = null;
//	}
//
//	public boolean isConnected() {
//		if (this.conn == null) {
//			return false;
//		}
//		return true;
//	}
//
//	public void connect() {
//		try {
//			Class.forName(driver);
//			String dbURL = url;
//			conn = DriverManager.getConnection(dbURL);
//			if (conn != null) {
//				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
//				stmt = conn.createStatement();
//				StdOutHandle.start("");
//				StdOutHandle.add("Configuring "
//								+ dm.getDriverName()
//								+ " version " + dm.getDatabaseProductVersion());
//			}
//		} catch (ClassNotFoundException ex) {
//			ex.printStackTrace();
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		}
//	}
//
//	public ResultSet read(String sql) {
//		try {
//			StdOutHandle.info("DATABASE READING: " + sql);
//			ResultSet rs = stmt.executeQuery(sql);
//			if (rs == null) {
//				StdOutHandle.info("NULL");
//			}
//			return rs;
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//			return null;
//		}
//	}
//
//	public Statement getStatement() {
//		return stmt;
//	}
//
//	public void write(String sql) {
//		try {
//			StdOutHandle.info("DATABASE WRITTING: " + sql);
//			stmt.executeUpdate(sql);
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		}
//	}
//
//	public void discconect() {
//		try {
//			stmt.close();
//			conn.close();
//			StdOutHandle.info("DISCONNECTION DONE");
//		} catch (SQLException ex) {
//			StdOutHandle.error("DISCONNECTION ERROR: " + ex);
//		}
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//}