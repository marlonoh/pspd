package org.gbross.pspd.database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import pervasive.jdbc.*;

import org.gbross.pspd.util.StdOutHandle;

public class PervasiveConnection {
	/**
	 *
	 * @author Kevin Arnold
	 */
	public PervasiveConnection() {
		getConnection();
	}
	@SuppressWarnings("finally")
	public Connection getConnection() {
		Connection conexion = null;
		try {
			Class.forName("com.pervasive.jdbc.v2.Driver");
			String url = "jdbc:pervasive://192.168.20.3:1583/cia01?transport=tcp";//jdbc:sqlserver://192.168.20.3;databaseName=Integracion;user=8401447;password=Cmgp1447;";
			conexion = DriverManager.getConnection(url);
			DatabaseMetaData dm = (DatabaseMetaData) conexion.getMetaData();
			Statement stmt = conexion.createStatement();
			StdOutHandle.add("Configuring PERVASIVE DB: "
					+ dm.getDriverName()
					+ " version " + dm.getDatabaseProductVersion());
		} catch (ClassNotFoundException ex) {
			System.err.println("Error1 en la Conexión con la BD "
					+ ex.getMessage());
			conexion = null;
		} catch (SQLException ex) {
			System.err.println("Error2 en la Conexión con la BD "
					+ ex.getMessage());
			conexion = null;
		} catch (Exception ex) {
			System.err.println("Error3 en la Conexión con la BD "
					+ ex.getMessage());
			conexion = null;
		} finally {
			return conexion;
		}
	}
}
